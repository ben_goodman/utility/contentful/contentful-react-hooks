import React, { type JSX, createContext, PropsWithChildren } from 'react'

type ContentfulPreviewProviderProps = PropsWithChildren<{
    enabled: () => boolean;
    previewApiHost: string;
    previewApiKey?: string;
    locale?: string;
}>;

type ContentfulPreviewContextProps = {
    previewApiHost: string;
    previewApiKey?: string;
    previewEnabled: boolean;
    locale: string;
}

const DEFAULT_CONTEXT: ContentfulPreviewContextProps = {
    previewApiHost: undefined,
    previewApiKey: undefined,
    previewEnabled: false,
    locale: 'en-US',
}

export const ContentfulPreviewContext =
    createContext<ContentfulPreviewContextProps>(DEFAULT_CONTEXT)

export const ContentfulPreviewProvider = ({
    children,
    previewApiKey,
    previewApiHost,
    enabled = () => false,
    locale = 'en-US',
}: ContentfulPreviewProviderProps): JSX.Element => {
    return (
        <ContentfulPreviewContext.Provider
            value={{
                previewApiKey,
                locale,
                previewEnabled: enabled(),
                previewApiHost
            }}
        >
            {children}
        </ContentfulPreviewContext.Provider>
    )
}