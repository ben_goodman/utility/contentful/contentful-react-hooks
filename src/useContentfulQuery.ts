/* eslint-disable @typescript-eslint/no-explicit-any */
import { useContext, useState } from 'react'
import { type DocumentNode } from 'graphql'
import { useQuery, type ApolloError, DefaultContext } from '@apollo/client'
import { ContentfulLivePreview, type ContentfulSubscribeConfig } from '@contentful/live-preview'

import { ContentfulPreviewContext } from './ContentfulPreviewProvider'


const initializeLivePreview = ({
    locale,
    data,
    query,
    callback
}: ContentfulSubscribeConfig) => {
    ContentfulLivePreview.init({ locale }).then(() => {
        console.info('Contentful Live Preview Initialized.')
        ContentfulLivePreview.subscribe({
            data,
            // query,
            callback
        })
    })
}

export interface ContentRequest<T> {
    entry: T;
    loading: boolean;
    error?: ApolloError;
}

export interface ContentfulQueryOptions<T> {
    variables?: object;
    reducer?: (gqlResponse: any) => T;
    init?: T;
    skip?: boolean;
    context?: DefaultContext;
}

export const useContentfulQuery = <T>(
    query: DocumentNode,
    {
        variables = {},
        reducer = (_) => _,
        init = undefined,
        skip = false,
        context = {}
    }: ContentfulQueryOptions<T>
) => {
    const {
        previewEnabled,
        locale
    } = useContext(ContentfulPreviewContext)

    const [contentResponse, setContentResponse] = useState<ContentRequest<T>>({
        loading: true,
        error: undefined,
        entry: init,
    })

    const onCompleted = (data: any) => {
        setContentResponse({
            loading: false,
            error: undefined,
            entry: reducer(data)
        })
        if (previewEnabled) {
            initializeLivePreview({
                locale,
                data,
                query,
                callback: (data) => {
                    console.info('Updating from live content.', { query, data })
                    setContentResponse({
                        loading: false,
                        error: undefined,
                        entry: reducer(data)
                    })
                }
            })
        }
    }

    const onError = (error: ApolloError) => {
        setContentResponse({
            loading: false,
            error,
            entry: init
        })
    }

    useQuery(query, {
        variables: {...variables, preview: previewEnabled },
        onCompleted,
        onError,
        skip,
        context
    })

    return contentResponse
}