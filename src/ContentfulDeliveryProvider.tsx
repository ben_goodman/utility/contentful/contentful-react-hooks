import React, { type JSX,  PropsWithChildren, useContext } from 'react'
import { ApolloClient, InMemoryCache, createHttpLink, ApolloProvider, type InMemoryCacheConfig, ApolloLink } from '@apollo/client'
import { setContext } from '@apollo/client/link/context'
import { ContentfulPreviewContext } from './ContentfulPreviewProvider'

type ContentfulGQLProviderProps = PropsWithChildren<{
    spaceId: string;
    envId: string;
    deliveryApiHost: string
    deliveryApiKey?: string;
    apolloCache?: InMemoryCacheConfig,
    apolloLink?: ApolloLink,
}>;


export const ContentfulDeliveryProvider = ({
    children,
    spaceId,
    envId,
    deliveryApiHost,
    deliveryApiKey = undefined,
    apolloCache = undefined,
    apolloLink = undefined,
}: ContentfulGQLProviderProps): JSX.Element => {

    const {
        previewApiKey,
        previewEnabled,
        previewApiHost,
    } = useContext(ContentfulPreviewContext)

    const apiHost = previewEnabled ? previewApiHost : deliveryApiHost

    const httpLink = createHttpLink({
        uri: `https://${apiHost}/content/v1/spaces/${spaceId}/environments/${envId}`,
    })

    const authLink = setContext((_, { headers }) => {
        let key = deliveryApiKey
        if (previewEnabled) {
            console.info('Using preview API key.')
            key = previewApiKey
        }
        if (!key) {
            return {}
        } else {
            return {
                headers: {
                    ...headers,
                    authorization: `Bearer ${key}` ,
                }
            }
        }
    })

    const client = new ApolloClient({
        link: ApolloLink.from([
            apolloLink,
            authLink,
            httpLink,
        ].filter(Boolean)),
        cache: new InMemoryCache(apolloCache),
    })

    return (
        <ApolloProvider client={client}>
            {children}
        </ApolloProvider>
    )
}