# Contentful React Hooks

A collection of React hooks for working with Contentful.  Features include:

- `useContentfulQuery`: A custom React hook for fetching content from Contentful using GraphQL queries. It must be used within the context of `ContentfulDeliveryProvider`.

- `ContentfulDeliveryProvider`: A provides an authorization context to all child components implementing `useContentfulQuery`.

<!-- - `ContentfulMockProvider`: Used in-place of `ContentfulDeliveryProvider`, all components within its context will receive a mocked GQL response for specified queries. -->

- `ContentfulPreviewProvider`: All components within its context will include `preview: true` in their GQL queries.


## Installation

Set the package's registry to the GitLab registry:

```bash
echo "@ben_goodman:registry=https://gitlab.com/api/v4/packages/npm/" >> .npmrc
```

Install the package with `npm i @ben_goodman/contentful-react-hooks`.

## API

### `ContentfulDeliveryProvider`

The `ContentfulDeliveryProvider` component is used to provide a Contentful API authorization context to your application. It takes the following props:

| Prop | Type | Default | Description |
| --- | --- | --- | --- |
|`deliveryApiKey` |`string`| - | Your Contentful API keys.
|`spaceId` |`string`| - | Your Contentful space ID. |
|`envId` |`string`| - | Your Contentful environment ID (if applicable). |
|`apiHost` |`string?`| 'graphql.contentful.com' | The Contentful API host. |
|`apolloCache` | InMemoryCacheConfig? | `undefined` | An optional Apollo cache configuration. |
|`apolloLink` | ApolloLink? | `undefined` | An optional Apollo link. |

Example:
```tsx
import React from 'react';
import { ContentfulDeliveryProvider } from '@ben_goodman/react-contentful-hooks';

function App() {
  return (
    <ContentfulDeliveryProvider
        spaceId={process.env.CONTENTFUL_SPACE_ID!}
        envId={process.env.CONTENTFUL_ENV_ID!}
        deliveryApiKey={process.env.CONTENTFUL_DELIVERY_TOKEN!}
    >
      {/* Your application content */}
    </ContentfulDeliveryProvider>
  );
}

export default App;
```

### `ContentfulPreviewProvider`

When encapsulating the delivery provider's context, the `ContentfulPreviewProvider` component is used to show preview content from Contentful It takes the following props:

| Property | Type | Default | Description |
| --- | --- | --- | --- |
|`previewApiKey` |`string`| `null` | Your Contentful preview API key. |
|`enabled` |`boolean?`| `true` | Whether or not preview mode is enabled. |

Example:
```tsx
import React from 'react';
import {
  ContentfulDeliveryProvider,
  ContentfulPreviewProvider
} from '@ben_goodman/react-contentful-hooks';


function App() {
  return (
    <ContentfulPreviewProvider
        previewApiKey={process.env.CONTENTFUL_PREVIEW_TOKEN!}
        enabled={true}
    >
        <ContentfulDeliveryProvider
            spaceId={process.env.CONTENTFUL_SPACE_ID!}
            envId={process.env.CONTENTFUL_ENV_ID!}
            deliveryApiKey={process.env.CONTENTFUL_DELIVERY_TOKEN!}
        >
            {/* Your application content */}
        </ContentfulDeliveryProvider>
    </ContentfulPreviewProvider>
  );
}

export default App;
```


### `useContentfulQuery`

The `useContentfulQuery` hook allows you to fetch content from Contentful using GraphQL queries. It returns the result of your query, along with loading and error states. It must be used within the context of `ContentfulDeliveryProvider`.

```ts
useContentfulQuery: <T>(
  query: DocumentNode,
  { variables, reducer, init }: ContentfulQueryOptions<T>
) => ContentRequest<T>
```

#### `ContentfulQueryOptions`

| Property | Type | Default | Description |
| --- | --- | --- | --- |
|`variables` |`object`| `{}` | An object containing variables to be passed to the GraphQL query. |
|`reducer` |`(data: any) => T`| `(_) => _` | A callback that takes the result of the GraphQL query and resolves a subset of the data. |
|`init` |`T`| `undefined` | An initial value for the query result. |
|`skip` |`boolean`| `false` | Whether or not to skip the query. |

```ts
ContentfulQueryOptions<T> {
    variables?: object;
    reducer?: (gqlResponse: any) => T;
    init?: T;
}
```

#### `ContentRequest`

```ts
ContentRequest<T> {
    entry: T;
    loading: boolean;
    error?: Error;
}
```

| Property | Type | Description |
| --- | --- | --- |
|`entry` |`T`| The result of the _reduced_ GraphQL query. |
|`loading` |`boolean`| Whether or not the query is loading. |
|`error` |`ApolloError`| An error object if the query failed. |


Example:
```tsx
import React from 'react';
import { useContentfulQuery } from '@ben_goodman/react-contentful-hooks';

const query = gql`
query GetNamedArticle($canonicalTitle: String, $preview: Boolean) {
	blogEntryCollection(where:{canonicalTitle: $canonicalTitle}, preview: $preview) {
    items {
      __typename
      canonicalTitle
      sys {
        __typename
        id
      }
    }
  }
}
`

const reducer = (resp: any): ArticleLookup => {
    // reduces the query response to the contentful id
    // {
    //     "data": {
    //       "blogEntryCollection": {
    //         "items": [
    //           {
    //             "sys": {
    //               "id": "57mLg9Sgn7CHqeXEMi10jF"
    //             }
    //           }
    //         ]
    //       }
    //     }
    //   }
    const data = resp.blogEntryCollection.items

    return {
        contentId: data[0]?.sys.id,
        canonicalTitle: data[0]?.canonicalTitle
    }
}

const App = () => {
  const {
      entry,
      loading,
      error,
  } = useContentfulQuery(
    query,
    {reducer, variables: {canonicalTitle: 'foo'}}
  )

  if (loading) {
    return <div>Loading...</div>;
  }

  if (error) {
    return <div>Error!</div>;
  }

  return (
    <>
      <h1>{entry.canonicalTitle}</h1>
      ...
    </>
  )
}